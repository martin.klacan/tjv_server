package cz.cvut.fit.tjv;

import cz.cvut.fit.tjv.dto.TrainCreateDTO;
import cz.cvut.fit.tjv.dto.TrainDTO;
import cz.cvut.fit.tjv.repository.TrainRepository;
import cz.cvut.fit.tjv.service.TrainService;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TjvServerApplicationTests {

    private final TrainService trainService;
    private static TrainDTO result;

    @Autowired
    TjvServerApplicationTests(TrainService trainService) {
        this.trainService = trainService;
    }


    @Test
    void test() throws Exception {
        TrainCreateDTO trainCreateDTO = new TrainCreateDTO();
        trainCreateDTO.setName("test");
        trainCreateDTO.setType("Ex");
        result = trainService.create(trainCreateDTO);
        assert(result.getName().equals("test"));
        assert (result.getType().equals("Ex"));
        trainService.delete(result.getId());
    }

}
