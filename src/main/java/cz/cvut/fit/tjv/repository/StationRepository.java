package cz.cvut.fit.tjv.repository;


import cz.cvut.fit.tjv.entity.Station;
import cz.cvut.fit.tjv.entity.Train;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StationRepository extends JpaRepository<Station, Integer> {

    @Query("SELECT s FROM Station s JOIN s.trains train WHERE train.id = :id")
    List<Station> findAllByTrain(int id);
}
