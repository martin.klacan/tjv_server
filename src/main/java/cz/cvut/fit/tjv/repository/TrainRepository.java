package cz.cvut.fit.tjv.repository;

import cz.cvut.fit.tjv.entity.Train;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainRepository extends JpaRepository<Train, Integer> {

    @Query("SELECT t FROM Train t JOIN t.lines line WHERE line.id = :id")
    List<Train> findAllByLine(int id);
}
