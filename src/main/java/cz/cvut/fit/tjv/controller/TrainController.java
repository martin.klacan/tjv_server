package cz.cvut.fit.tjv.controller;

import cz.cvut.fit.tjv.dto.TrainCreateDTO;
import cz.cvut.fit.tjv.dto.TrainDTO;
import cz.cvut.fit.tjv.service.TrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class TrainController {

    private final TrainService trainService;

    @Autowired
    public TrainController(TrainService trainService) {
        this.trainService = trainService;
    }

    @GetMapping("/train")
    List<TrainDTO> all() {
        return trainService.findAll();
    }

    @GetMapping("/train/{id}")
    TrainDTO byId(@PathVariable int id) {
        return trainService.findByIdAsDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/train")
    TrainDTO save(@RequestBody TrainCreateDTO train) throws Exception {
        return trainService.create(train);
    }

    @PutMapping("/train/{id}")
    TrainDTO save(@PathVariable int id, @RequestBody TrainDTO train) throws Exception {
        return trainService.update(id, train);
    }

    @DeleteMapping("/train/{id}")
    void delete(@PathVariable int id) {
        trainService.delete(id);
    }

    @GetMapping(value = "/train", params = {"lineId"})
    List<TrainDTO> byLineId(@RequestParam int lineId) {
        return trainService.findAllByLine(lineId);
    }

    @PutMapping(value = "/train/{id}", params = {"lineId"})
    TrainDTO save(@PathVariable int id, @RequestParam int lineId) throws Exception {
        return trainService.addLine(id, lineId);
    }

    @PutMapping(value = "/train/{id}", params = {"lineId","r=t"})
    TrainDTO removeLine(@PathVariable int id, @RequestParam int lineId) throws Exception {
        return trainService.removeLine(id, lineId);
    }
}
