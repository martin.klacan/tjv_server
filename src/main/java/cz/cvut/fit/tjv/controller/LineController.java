package cz.cvut.fit.tjv.controller;

import cz.cvut.fit.tjv.dto.LineCreateDTO;
import cz.cvut.fit.tjv.dto.LineDTO;
import cz.cvut.fit.tjv.service.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class LineController {

    private final LineService lineService;

    @Autowired
    public LineController(LineService lineService) {
        this.lineService = lineService;
    }

    @GetMapping("/line")
    List<LineDTO> all() {
        return lineService.findAll();
    }

    @GetMapping("/line/{id}")
    LineDTO byId(@PathVariable int id) {
        return lineService.findByIdAsDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/line")
    LineDTO save(@RequestBody LineCreateDTO line) {
        return lineService.create(line);
    }

    @PutMapping("/line/{id}")
    LineDTO save(@PathVariable int id, @RequestBody LineDTO line) throws Exception {
        return lineService.update(id, line);
    }

    @DeleteMapping("/line/{id}")
    void delete(@PathVariable int id) {
        lineService.delete(id);
    }

}
