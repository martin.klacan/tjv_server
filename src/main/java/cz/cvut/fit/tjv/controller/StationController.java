package cz.cvut.fit.tjv.controller;

import cz.cvut.fit.tjv.dto.StationCreateDTO;
import cz.cvut.fit.tjv.dto.StationDTO;
import cz.cvut.fit.tjv.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class StationController {

    private final StationService stationService;

    @Autowired
    public StationController(StationService stationService) {
        this.stationService = stationService;
    }

    @GetMapping("/station")
    List<StationDTO> all() {
        return stationService.findAll();
    }

    @GetMapping("/station/{id}")
    StationDTO byId(@PathVariable int id) {
        return stationService.findByIdAsDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/station")
    StationDTO save(@RequestBody StationCreateDTO station) {
        return stationService.create(station);
    }

    @PutMapping("/station/{id}")
    StationDTO save(@PathVariable int id, @RequestBody StationDTO station) throws Exception {
        return stationService.update(id, station);
    }

    @DeleteMapping("/station/{id}")
    void delete(@PathVariable int id) {
        stationService.delete(id);
    }

    @PutMapping(value = "/station/{id}", params = {"trainId"})
    StationDTO save(@PathVariable int id, @RequestParam int trainId) throws Exception {
        return stationService.addTrain(id, trainId);
    }

    @PutMapping(value = "/station/{id}", params = {"trainId","r=t"})
    StationDTO removeLine(@PathVariable int id, @RequestParam int trainId) throws Exception {
        return stationService.removeTrain(id, trainId);
    }
}
