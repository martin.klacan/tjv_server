package cz.cvut.fit.tjv.entity;

import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Line {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String name;

    public Line() {}

    public Line(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
