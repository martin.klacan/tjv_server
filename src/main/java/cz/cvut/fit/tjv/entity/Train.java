package cz.cvut.fit.tjv.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;

@Entity
public class Train {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String name;

    @NotNull
    private String type;

    @ManyToMany
    @JoinTable(name = "train_line",
               joinColumns = @JoinColumn(name = "train_id"),
               inverseJoinColumns = @JoinColumn(name = "line_id")
        )
    private List<Line> lines;

    public Train(){

    }

    public Train(String name, String type, List<Line> lines) {
        this.name = name;
        this.type = type;
        this.lines = lines;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public List<Line> getLines() {
        return lines;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setLines(List<Line> lines) {
        this.lines = lines;
    }
}
