package cz.cvut.fit.tjv.dto;

public class LineCreateDTO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
