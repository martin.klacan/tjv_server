package cz.cvut.fit.tjv.dto;

import java.util.List;

public class TrainDTO {

    private final int id;
    private final String name;
    private final String type;
    private final List<Integer> lineIds;

    public TrainDTO(int id, String name, String type, List<Integer> lineIds) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.lineIds = lineIds;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public List<Integer> getLineIds() {
        return lineIds;
    }
}
