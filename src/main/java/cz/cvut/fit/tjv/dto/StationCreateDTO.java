package cz.cvut.fit.tjv.dto;

public class StationCreateDTO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
