package cz.cvut.fit.tjv.dto;

public class LineDTO {

    private final int id;
    private final String name;

    public LineDTO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
