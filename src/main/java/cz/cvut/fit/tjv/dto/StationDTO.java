package cz.cvut.fit.tjv.dto;

import java.util.List;

public class StationDTO {

    private final int id;
    private final String name;
    private final List<Integer> trainIds;

    public StationDTO(int id, String name, List<Integer> trainIds) {
        this.id = id;
        this.name = name;
        this.trainIds = trainIds;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Integer> getTrainIds() {
        return trainIds;
    }
}
