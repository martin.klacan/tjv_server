package cz.cvut.fit.tjv.service;


import cz.cvut.fit.tjv.dto.StationCreateDTO;
import cz.cvut.fit.tjv.dto.StationDTO;
import cz.cvut.fit.tjv.dto.TrainDTO;
import cz.cvut.fit.tjv.entity.Station;
import cz.cvut.fit.tjv.entity.Train;
import cz.cvut.fit.tjv.repository.StationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StationService {

    private final StationRepository stationRepository;
    private final TrainService trainService;

    @Autowired
    public StationService(StationRepository stationRepository, TrainService trainService) {

        this.stationRepository = stationRepository;
        this.trainService = trainService;
    }

    public List<StationDTO> findAll() {
        return stationRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    public List<Station> findByIds(List<Integer> ids) {
        return stationRepository.findAllById(ids);
    }

    public Optional<Station> findById(int id) {
        return stationRepository.findById(id);
    }

    public Optional<StationDTO> findByIdAsDTO(int id) {
        return toDTO(findById(id));
    }

    public StationDTO create(StationCreateDTO stationDTO) {
        return toDTO(stationRepository.save(
                new Station(
                        stationDTO.getName(),
                        Collections.emptyList()
                )
        ));
    }

    public void delete(int id) {
        stationRepository.delete(findById(id).get());
    }

    @Transactional
    public StationDTO update(int id, StationDTO stationDTO) throws Exception {
        Optional<Station> optionalStation = stationRepository.findById(id);
        if (optionalStation.isEmpty())
            throw new Exception("no such station");
        Station station = optionalStation.get();
        station.setName(stationDTO.getName());
        return toDTO(station);
    }

    private StationDTO toDTO(Station station) {

        return new StationDTO(
                station.getId(),
                station.getName(),
                station.getTrains().stream().map(Train::getId).collect(Collectors.toList())
        );

    }

    private Optional<StationDTO> toDTO(Optional<Station> station) {
        if (station.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(station.get()));
    }

    public List<StationDTO> findAllByTrain(int id) {
        return stationRepository.findAllByTrain(id)
                .stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @Transactional
    public StationDTO addTrain(int id, int trainId) throws Exception {
        Optional<Station> optionalStation = findById(id);
        if (optionalStation.isEmpty())
            throw new Exception("no such station");
        Station station = optionalStation.get();
        station.getTrains().add(trainService.findById(trainId).get());
        return toDTO(station);
    }

    @Transactional
    public StationDTO removeTrain(int id, int trainId) throws Exception {
        Optional<Station> optionalStation = findById(id);
        if (optionalStation.isEmpty())
            throw new Exception("no such station");
        Station station = optionalStation.get();
        station.getTrains().remove(trainService.findById(trainId).get());
        return toDTO(station);
    }
}
