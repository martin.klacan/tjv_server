package cz.cvut.fit.tjv.service;

import cz.cvut.fit.tjv.dto.TrainCreateDTO;
import cz.cvut.fit.tjv.dto.TrainDTO;
import cz.cvut.fit.tjv.entity.Line;
import cz.cvut.fit.tjv.entity.Train;
import cz.cvut.fit.tjv.repository.TrainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TrainService {

    private final TrainRepository trainRepository;
    private final LineService lineService;

    @Autowired
    public TrainService(TrainRepository trainRepository, LineService lineService) {
        this.trainRepository = trainRepository;
        this.lineService = lineService;
    }

    public List<TrainDTO> findAll() {
        return trainRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    public Optional<Train> findById(int id) {
        return trainRepository.findById(id);
    }

    public Optional<TrainDTO> findByIdAsDTO(int id) {
        return toDTO(findById(id));
    }

    @Transactional
    public TrainDTO create(TrainCreateDTO trainDTO) throws Exception {
        Train train = new Train(trainDTO.getName(), trainDTO.getType(), Collections.emptyList());
        return toDTO(trainRepository.save(train));
    }

    @Transactional
    public TrainDTO update(int id, TrainDTO trainDTO) throws Exception {
        Optional<Train> optionalTrain = findById(id);
        if (optionalTrain.isEmpty())
            throw new Exception("no such train");
        Train train = optionalTrain.get();
        train.setType(trainDTO.getType());
        train.setName(trainDTO.getName());
        return toDTO(train);
    }

    public void delete(int id) {
        trainRepository.delete(findById(id).get());
    }

    public List<TrainDTO> findAllByLine(int id) {
        return trainRepository.findAllByLine(id)
                .stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    private TrainDTO toDTO(Train train) {
        return new TrainDTO(
                train.getId(),
                train.getName(),
                train.getType(),
                train.getLines().stream().map(Line::getId).collect(Collectors.toList())
        );
    }

    private Optional<TrainDTO> toDTO(Optional<Train> train) {
        if (train.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(train.get()));
    }

    @Transactional
    public TrainDTO addLine(int id, int lineId) throws Exception {
        Optional<Train> optionalTrain = findById(id);
        if (optionalTrain.isEmpty())
            throw new Exception("no such train");
        Train train = optionalTrain.get();
        train.getLines().add(lineService.findById(lineId).get());
        return toDTO(train);
    }

    @Transactional
    public TrainDTO removeLine(int id, int lineId) throws Exception {
        Optional<Train> optionalTrain = findById(id);
        if (optionalTrain.isEmpty())
            throw new Exception("no such train");
        Train train = optionalTrain.get();
        train.getLines().remove(lineService.findById(lineId).get());
        return toDTO(train);
    }
}