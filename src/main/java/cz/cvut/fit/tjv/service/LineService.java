package cz.cvut.fit.tjv.service;

import cz.cvut.fit.tjv.dto.LineCreateDTO;
import cz.cvut.fit.tjv.dto.LineDTO;
import cz.cvut.fit.tjv.entity.Line;
import cz.cvut.fit.tjv.repository.LineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LineService {

    private final LineRepository lineRepository;

    @Autowired
    public LineService(LineRepository lineRepository) {
        this.lineRepository = lineRepository;
    }

    public List<LineDTO> findAll() {
        return lineRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    public List<Line> findByIds(List<Integer> ids) {
        return lineRepository.findAllById(ids);
    }

    public Optional<Line> findById(int id) {
        return lineRepository.findById(id);
    }

    public Optional<LineDTO> findByIdAsDTO(int id) {
        return toDTO(findById(id));
    }

    public LineDTO create(LineCreateDTO lineDTO) {
        return toDTO(lineRepository.save(
                new Line(
                        lineDTO.getName()
                )
        ));
    }

    public void delete(int id) {
        lineRepository.delete(findById(id).get());
    }

    @Transactional
    public LineDTO update(int id, LineDTO lineDTO) throws Exception {
        Optional<Line> optionalLine = lineRepository.findById(id);
        if (optionalLine.isEmpty())
            throw new Exception("no such line");
        Line line = optionalLine.get();
        line.setName(lineDTO.getName());
        return toDTO(line);
    }

    private LineDTO toDTO(Line line) {

        return new LineDTO(line.getId(), line.getName());
    }

    private Optional<LineDTO> toDTO(Optional<Line> line) {
        if (line.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(line.get()));
    }
}
